# README #

This is a repo with love2d built on an old Debian Wheezy inside a VBox machine.

### What is this repository for? ###

* Have x86 and x86_64 builds
* love 10.2 with libluajit2.1 (beta 2) and SDL 2.0.5

### How do I get set up? ###

* There are 2 folders "32" and "64", guess which is for which architecture ;)
* Just copy contents of any of those anywhere and add your *.love to <arch>/usr/bin/love from a package
* All dependencies are shipped with this package

### Contribution guidelines ###

* Download and tell me if those builds work

### Who do I talk to? ###

* Repo owner or admin
* love2d community members (https://love2d.org/forum)
